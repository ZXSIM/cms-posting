import React, { Component } from "react";
import PostingPage from "./page/posting-page/PostingPage";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import "./App.css";

class App extends Component {
  theme = createMuiTheme({
    typography: {
      useNextVariants: true
    }
  });
  render() {
    return (
      <MuiThemeProvider theme={this.theme}>
        <div className="App">
          <PostingPage />
        </div>
      </MuiThemeProvider>
    );
  }
}

export default App;
