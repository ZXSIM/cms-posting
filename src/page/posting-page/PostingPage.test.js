import React from "react";
import PostPage from "./PostingPage";
import {
  render,
  waitForElement,
  getByText,
  fireEvent
} from "react-testing-library";

// beforeEach(()=>{
//   jest.clear
// })

describe("PostPage", () => {
  it("should change the url based on the title", () => {
    const { container } = render(<PostPage />);
    const title = container.querySelector("#postInputTitle");
    const url = container.querySelector("#postInputUrl");

    fireEvent.change(title, { target: { value: "test" } });

    expect(url.value).toEqual("test");
  });

  it("should change the url", () => {
    const { container } = render(<PostPage />);
    const url = container.querySelector("#postInputUrl");

    fireEvent.change(url, { target: { value: "Hello World" } });

    expect(url.value).toEqual("Hello World");
  });
});
